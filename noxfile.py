"""Nox configuration."""

import nox
from nox.command import CommandFailed

nox.options.sessions = ["tests"]


@nox.session(python=["3.9", "3.10", "3.11", "3.12"])
def tests(session: nox.Session) -> None:
    """Run pytest across multiple python versions."""

    try:
        session.install('.[test]')
        session.run('pytest')
    except CommandFailed:
        if session.python == "3.12":
            session.skip("Known problems with dependencies in Python 3.12")
        raise
