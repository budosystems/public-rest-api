#
#  Copyright (c) 2021.  Budo Systems
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
#

"""
Sample REST API for Budo Systems.

This is meant to be a proof of concept, not a production ready implementation.
"""

from __future__ import annotations

import importlib
import inspect
import pkgutil
from uuid import UUID
from types import ModuleType, SimpleNamespace
from typing import Any, Union
from typing_extensions import TypeAlias

from sanic import Sanic
from sanic.config import Config
from sanic.blueprints import Blueprint
from sanic.blueprint_group import BlueprintGroup
from sanic.views import HTTPMethodView
from sanic.response import json, HTTPResponse
from sanic.request import Request

from attrs import asdict, evolve, fields_dict

from parallel_hierarchy import ParallelFactory

from budosystems import models
from budosystems.models import core
from budosystems.storage.repository import Repository, SaveOption, EntityReadFailed, RepositoryNotAccessible

# app = Sanic("BudoSystems_API", strict_slashes=True)

bpg_api = Blueprint.group(url_prefix="/api", version=1, strict_slashes=True)


# class BudoContext(SimpleNamespace):
#     """Context for requests and the main app."""
BudoContext: TypeAlias = SimpleNamespace


class BudoConfig(Config):
    """Configuration for the main app."""
    repository: Repository


# class BudoRequest(Request[Sanic[BudoConfig, BudoContext], BudoContext]):
#     pass
BudoRequest: TypeAlias = Request[Sanic[BudoConfig, BudoContext], BudoContext]


class SanicBaseView(HTTPMethodView):
    """Base class for REST method views."""

    source: type[core.Entity]

    def __init_subclass__(cls, **kwargs: Any) -> None:  # pylint: disable=signature-differs
        _passthrough_kw = [
                "attach", "uri", "methods", "hosts", "strict_slashes", "version",
                "name", "stream", "version_prefix",
        ]
        _passthrough_args = {k: v for k, v in kwargs.items() if k in _passthrough_kw}
        super().__init_subclass__(**_passthrough_args)

        # This step is done by default for generated classes, but not by custom classes in the
        # View hierarchy.
        SViews.register(cls)

    async def _get_item(self, repo: Repository, key: Union[str, UUID]) -> core.Entity:
        if isinstance(key, str):
            try:
                key = UUID(key)
            except ValueError as e:
                if "slug" in fields_dict(self.source):
                    return repo.match(self.source, {"slug": key})[0]
                raise ValueError("key is not UUID and "
                                 f"{self.source.__name__} does not support slugs"
                                 ) from e

        item = repo.load(self.source, key)

        return item

    async def _get_list(self, repo: Repository) -> list[core.Entity]:
        items = repo.match(self.source, {})
        return items

    @staticmethod
    def _repo(request: BudoRequest) -> Repository:
        try:
            repo: Repository = request.app.config.repository
            return repo
        except AttributeError as e:
            raise RepositoryNotAccessible("Cannot access the repository from config.") from e

    async def get(self, request: BudoRequest, key: Union[str, UUID, None] = None) -> HTTPResponse:
        """
        Sanic processor for the HTTP GET method.

        :param request: The HTTP Request object provided by Sanic.
        :param key: The key of the object to operate on.
            If not provided, lists all objects for this route.
        :returns: A valid HTTP Response that will be sent to the requester.
        """
        repo = self._repo(request)
        count: Union[int, None] = None
        content: Union[dict[str, Any], list[dict[str, Any]]]
        if key:
            item = await self._get_item(repo, key)
            content = asdict(item)
        else:
            # send full listing
            items = await self._get_list(repo)
            content = [asdict(item) for item in items]
            count = len(content)

        body = {"status": 200, "content": content}
        if count is not None:
            body["count"] = count
        return json(body)

    async def post(self, request: BudoRequest) -> HTTPResponse:
        """
        Sanic processor for the HTTP POST method.

        :param request: The HTTP Request object provided by Sanic.
        :returns: A valid HTTP Response that will be sent to the requester.
        """
        repo = self._repo(request)

        data = await request.json
        item = self.source(**data)
        repo.save(item, save_option=SaveOption.create_only)
        return json({"status": "created"}, status=201)

    async def put(self, request: BudoRequest, key: Union[str, UUID]) -> HTTPResponse:
        """
        Sanic processor for the HTTP PUT method.

        :param request: The HTTP Request object provided by Sanic.
        :param key: The key of the object to operate on.
        :returns: A valid HTTP Response that will be sent to the requester.
        """
        repo = self._repo(request)

        data = await request.json
        try:
            item = await self._get_item(repo, key)
            item = evolve(item, **data)
        except EntityReadFailed:
            item = self.source(**data)
        repo.save(item, save_option=SaveOption.create_or_update)
        return json({"status": "updated"}, status=200)

    async def patch(self, request: BudoRequest, key: Union[str, UUID]) -> HTTPResponse:
        """
        Sanic processor for the HTTP PATCH method.

        :param request: The HTTP Request object provided by Sanic.
        :param key: The key of the object to operate on.
        :returns: A valid HTTP Response that will be sent to the requester.
        """
        repo = self._repo(request)

        data = await request.json
        item = await self._get_item(repo, key)
        item = evolve(item, **data)
        repo.save(item, save_option=SaveOption.update_only)
        return json({"status": "updated"}, status=200)

    async def delete(self, request: BudoRequest, key: Union[str, UUID]) -> HTTPResponse:
        """
        Sanic processor for the HTTP DELETE method.

        :param request: The HTTP Request object provided by Sanic.
        :param key: The key of the object to operate on.
        :returns: A valid HTTP Response that will be sent to the requester.
        """
        repo = self._repo(request)

        item = await self._get_item(repo, key)
        repo.delete(self.source, item.entity_id)
        return json({"status": "deleted"}, status=200)


SViews = ParallelFactory(core.Entity, SanicBaseView, suffix="View")
"""A repository and factory of Sanic Views."""


def generate_all_entity_views(mod: ModuleType, bpg: BlueprintGroup) -> None:
    """Processes all the Entity classes defined in `mod` and its submodules through the factory."""
    mod_name = mod.__name__.split(".")[-1]
    bpg_mod = Blueprint.group(url_prefix="/" + mod_name)
    bp = Blueprint(mod_name)
    view_added = False
    for mbr_name, mbr_obj in inspect.getmembers(mod):
        if isinstance(mbr_obj, type) and issubclass(mbr_obj, core.Entity):
            view_fn = SViews.get(mbr_obj).as_view()
            bp.add_route(view_fn, "/" + mbr_name.lower())
            view_added = True

    if view_added:
        bpg_mod.append(bp)

    if hasattr(mod, "__path__"):
        submods = pkgutil.walk_packages(mod.__path__)
        for _loader, name, _is_pkg in submods:
            submod = importlib.import_module(f"{mod.__name__}.{name}")
            generate_all_entity_views(submod, bpg_mod)

    bpg.append(bpg_mod)


generate_all_entity_views(models, bpg_api)
