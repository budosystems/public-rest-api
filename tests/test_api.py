import pytest

from sanic import Sanic
from budosystems.xtra.public_rest import api

def test_generate_all_entity_views():
    app = Sanic("TestViews", strict_slashes=True)
    app.blueprint(api.bpg_api)
    print()
    print("Blueprints:")
    for k, v in app.blueprints.items():
        print(f"  {k=}, {v=}")

    print("Routes:")
    for r in app.router.routes:
        print(r)
